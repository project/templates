********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Template Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Handles data,  logic, and UI of templates(s) which are used by other
modules.
 
Templates are  stored in  the database,  and  are  used for specific
purposes, e.g. email, pdf.

The template can be used for most purposes,  and  includes following
information:

Type: Type of service for which this template is used. Ties template
to the module using template.module.

Name: Name of the template

Markup: How the template will be used, e.g 'plain text', HTML, SVG.

Format: Input format being used. For instance PHP  could be used as
the input format  to create a HTML or plain text template, allowing
the template to include some logic.

This module provides an API, don't install it unless required by another module.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire publication directory into your Drupal modules/
   directory.

2. Enable the publication module by navigating to:

     administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/template
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F




********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>


